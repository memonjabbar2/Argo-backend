const express = require('express')
const bodyParser = require('body-parser');
const mongodb = require('mongodb').MongoClient;
var mongoose = require('mongoose');
var app = express();
var fs = require('fs');
var csv = require("fast-csv");
const loanSchema = require("../models/loanComparisonSchema.js")
var stream = fs.createReadStream("./loanComparision_files/loan_comparison_of_port_2012.csv")

var credentials = require('./../key.js')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
  }));

  module.exports = {
      addLoan : async (req , res , next) => {
          try {
            csv
            .fromStream(stream,{headers : true})
            .on("data", (data) => {
               mongodb.connect(credentials.url,{ 
                useNewUrlParser: true,socketTimeoutMS: 30000,
                keepAlive: true,
                reconnectTries: 30000 },function(err , database){
                   if(err) 
                   console.log(err)
                   var loanData = new loanSchema({
                    port_name : data.port_name,
                    port_year : data.port_year,
                    state : data.state,
                    total_loan_counts : data.total_loan_counts,
                    total_orig_upb : data.total_orig_upb,
                    avg_orig_upb : data.avg_orig_upb,
                    avg_ltv : data.avg_ltv,
                    avg_fico : data.avg_fico,
                    avg_cltv : data.avg_cltv,
                    avg_dti : data.avg_dti,
                    avg_int_rt : data.avg_int_rt,
                    avg_mi_pct : data.avg_mi_pct,
                    avg_loan_age : data.avg_loan_age,
                    // flag_fthb : {
                    //     N_counts : data.flag_fthb_N,
                    //     Y_counts : data.flag_fthb_Y
                    //     //X_counts : data.flag_fthb_X
                    // },
                    // occpy_sts : {
                    //     I_counts : data.occpy_sts_I,
                    //     P_counts : data.occpy_sts_P,
                    //     S_counts : data.occpy_sts_S
                    // },
                    // prop_type : {
                    //     CO_counts : data.CO_counts,
                    //     CO_counts : data.CO_counts,
                    //     CO_counts : data.CO_counts,
                    //     CO_counts : data.CO_counts,
                    // }

                   })
                    //await has to be added
                    loanData.save(function(error , res) {
                        console.log("Data has been saved ");
                        if(error) {
                          console.log(error)
                        }
                 })  
             })
           
            })
            .on("error" ,function(err) {
                console.log(err);
            })
            .on("end", function(){
                res.send({message : "Successfully Uploaded"})
                console.log("done");
            });
          }catch(error) {
              console.log(error)
              next(error);
          }
      },
      loanComparison : async (req , res, next) => {
            console.log("Loan Comparison");
            var port_name = req.body.port_name;
            var port_name1 = req.body.port_name1;
            var chars = req.body.chars;
    
            if(!port_name) {
              return res.status(404).send("Plz select the portfolio Name");
            }
            if(!chars) {
              return res.status(404).send("Plz select the scenario");
            }
            if(!port_name1) {
              return res.status(404).send("Plz select the portfolio Name");
            }
            
            console.log("port_name" , port_name);
            console.log("port name 1" , port_name1)
            console.log("scenario" , chars);
            var portfolio1 = [];
            var portfolio2 = [];
            const loanComparisonData = await loanSchema.find({"port_name" : port_name}).sort('state')
            if(!loanComparisonData) {
                return res.status(403).send({error : "loanComparison Not Found!"});
              }
              await loanComparisonData.forEach((data) => {
                  if(data[chars]) {
                      portfolio1.push({data : data[chars], state : data.state})
                  }
                  
              })
              //console.log(portfolio1)
              const loanComparisonData1 = await loanSchema.find({"port_name" : port_name1}).sort('state')
              if(!loanComparisonData1) {
                  return res.status(403).send({error : "loanComparison Not Found!"});
                }
                await loanComparisonData1.forEach((data) => {
                    if(data[chars]) {
                        portfolio2.push({data : data[chars], state : data.state})
                    }
                    
                })
                console.log(portfolio2)

                await portfolio1.forEach((subtract) => {
                    portfolio2.forEach((subtract1) => {
                        if(subtract.state == subtract1.state) {
                            subtract.data = (subtract.data - subtract1.data)
                        }
                      
                    })
                  })
                  console.log(portfolio1)
                  res.send(portfolio1)

      }
  }