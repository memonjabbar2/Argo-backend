const JWT = require('jsonwebtoken');
const User = require("./../models/user.js")
const key = require('./../key.js')
var crypto = require('crypto');
var async = require('async');
var nodemailer = require('nodemailer');
var credentials = require('./../key.js');
signToken = user => {
    return JWT.sign({
        iss : 'mortage',
        sub : user.id,
        iat : new Date().getTime(), //Current Time
        exp : new Date().setDate(new Date().getDate() + 1) //Current Time + 1 day ahead
    } ,key.secret)
}
module.exports = {
    signUp : async (req , res , next) => {
        //Email and Password
        try {
            console.log("usercontroller signup called");
            const email = req.value.body.email;
            const password = req.value.body.password;
            //check if there is a user same email
            const foundUser = await User.findOne({email : email})
            if(foundUser) {
                return res.status(403).send({error : "Email Already Exist.Try to Signup With Different Email!"});
            }
            //create a new user
            const newUser = new User({
                email : email,
                password : password
                
            })
           await newUser.save(); 

            //Generate the token
            const token = signToken(newUser)

           //Respond with Token
           res.status(200).json({token : token})

        }
        catch(error) {
            console.log(error)
            next(error)
        }
       
    },
    signIn : async (req , res , next) => {
        //Generate the Token
        try {
            
            //console.log('request user' , req.user)
            const token = signToken(req.user)
            res.status(200).json({token : token})
            console.log("Successful Login")
            
        }
        catch(error) {
            console.log(error)
            next(error)
        }
        
     },
     secret : async (req , res , next) => {
         try {
            console.log("usercontroller secret called")
            res.send({secret : "Resource"})
         }
         catch(error) {
            console.log(error)
            next(error)
        }
        
     },
     forgot : async (req ,res , next) => {
         try {
            console.log("Forgot Password");
            async.waterfall([
                function(done) {
                  crypto.randomBytes(20, function(err, buf) {
                    var token = buf.toString('hex');
                    done(err, token);
                  });
                },
                function(token, done) {
                  User.findOne({ email: req.body.email }, function(err, user) {
                    if (!user) {
                     // req.flash('error', 'No account with that email address exists.');
                      return res.send({message : "No account with that email address exists."});
                    }
            
                    user.resetPasswordToken = token;
                    user.resetPasswordExpires = Date.now() + 3600000; // 1 hour
            
                    user.save(function(err) {
                      done(err, token, user);
                      res.send({token : token});
                    });
                  });
                },
                function(token, user, done) {
                  var smtpTransport = nodemailer.createTransport({
                    service: 'Gmail',
                    host: "smtp.gmail.com",
                    secure: false,
                    port: 465,
                    auth: {
                        user: credentials.email,
                        pass: credentials.password
                
                      },
                  });
                  var mailOptions = {
                    to: user.email,
                    from: credentials.email,
                    subject: 'Node.js Password Reset',
                    text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                      'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                      'http://' + req.headers.host + '/reset/' + token + '\n\n' +
                      'If you did not request this, please ignore this email and your password will remain unchanged.\n'
                  };
                  smtpTransport.sendMail(mailOptions, function(err) {
                    //req.flash('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
                    done(err, 'done');
                  });
                }
              ], function(err) {
                if (err) return next(err);
                //res.redirect('/forgot');
              });
         }catch(error) {
             console.log(error);
             next(error);
         }
     },
     reset : async (req , res , next) => {
         console.log("in reset function")
         const token = req.body.token;
         console.log(token)
        async.waterfall([
            function(done) {
              User.findOne({ resetPasswordToken: token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
                if (!user) {
                    console.log("User not found")
                    return "User not found"
                  //req.flash('error', 'Password reset token is invalid or has expired.');
                  // res.redirect('back');
                }
                console.log(user)
                user.password = req.body.password;
                console.log(user.password)
                user.resetPasswordToken = undefined;
                user.resetPasswordExpires = undefined;
        
                user.save(function(err) {
                    done(err, user);
                    console.log("saved");
                    //res.send({token : token});
                  });
              });
            },
            function(user, done) {
              var smtpTransport = nodemailer.createTransport({
                service: 'Gmail',
                    host: "smtp.gmail.com",
                    secure: false,
                    port: 465,
                    auth: {
                        user: credentials.email,
                        pass: credentials.password
                
                      },
              });
              var mailOptions = {
                to: user.email,
                from: credentials.email,
                subject: 'Your password has been changed',
                text: 'Hello,\n\n' +
                  'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
              };
              smtpTransport.sendMail(mailOptions, function(err) {
               // req.flash('success', 'Success! Your password has been changed.');
               console.log("Success! Your password has been changed.")
                done(err);
              });
            }
          ], function(err) {
              if(err) {
                  console.log(err)

              }
              console.log("done")

           // res.redirect('/');
          });
     }
}