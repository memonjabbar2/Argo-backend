const express = require('express')
const bodyParser = require('body-parser');
const mongodb = require('mongodb').MongoClient;
var mongoose = require('mongoose');
var app = express();
var fs = require('fs');
var csv = require("fast-csv");
const simulationSchema = require("../models/simulationSchema.js")
var credentials = require('./../key.js')

const {Storage} = require('@google-cloud/storage');
const CLOUD_BUCKET = "portfolio_csv";
const googleCloudStorage =  new Storage({
    projectId: process.env.GCLOUD_STORAGE_BUCKET ||"argo-mortgage",
    keyFilename: process.env.GCLOUD_KEY_FILE || "./credentials.json"
  });
  
  const bucket = googleCloudStorage.bucket(CLOUD_BUCKET);



  var dataArray= [];
  var option ={
    useNewUrlParser: true,
    socketTimeoutMS: 30000,
    keepAlive: true,
    reconnectTries: 30000,
    useNewUrlParser: true,
    reconnectTries: 60,
    reconnectInterval: 1000,
    poolSize: 10,
    bufferMaxEntries: 0
}


  module.exports = {
    addsimulation : async (req , res, next) => {
            var simulationid = Math.random().toString(36).substr(2, 9)
                    console.log("Simulation ID",simulationid)
                    console.log(req.file)
             var url;
             //Getting input File
            if (!req.file) {
                res.status(400).send("No file uploaded.");
                return;
              }
            
              // Create a new blob in the bucket and upload the file data.
               const blob = bucket.file(req.file.originalname);
            
              // Make sure to set the contentType metadata for the browser to be able
              // to render the image instead of downloading the file (default behavior)
              const blobStream = blob.createWriteStream({
                metadata: {
                  contentType: req.file.mimetype
                }
              });
            
              blobStream.on("error", err => {
                next(err);
                return;
              });
            
             await blobStream.on("finish", () => {
                // The public URL can be used to directly access the file via HTTP.
                const publicUrl = `https://storage.googleapis.com/${bucket.name}/${blob.name}`;
            
                // Make the image public to the web (since we'll be displaying it in browser)
                blob.makePublic().then(() => {
                  //res.status(200).send(`Success!\n File uploaded to ${publicUrl}`);
                  console.log(`Success!\n File uploaded to ${publicUrl}`)
                  url = publicUrl
                }).then(() => {
                    var simulation = req.body.simulation_name;
                    if(!simulation) {
                            return res.status(404).send({message : "Simulation Name field is Required"});
                        }
                    console.log("Simulation name==>" , simulation);        
                    
                    //Saving the file to local Storage
                    fs.writeFile("upload_simulation_db.csv", req.file.buffer, function (err) {
                        if (err) {
                            return console.log(err);
                        }
                        console.log("The file was saved! on localhost");
                          
                    //Adding file Data to Array for inserting into DB.
                    var port_file =  fs.createReadStream("upload_simulation_db.csv")
                    csv
                        .fromStream(port_file,{headers : true})
                        .on("data", (data) => {
                       // console.log(data)
                       dataArray.push({simulation_name : simulation, simulation_id : "htjh8ix6o",simulation:Number(data.simulation),
                        housing_price_index:Number(data.housing_price_index), unemployment_rate:Number(data.unemployment_rate),
                        year_30_mortage_rate:Number(data.year_30_mortage_rate),sims_name:data.sims_name, year : Number(data.year),})
                       //console.log(dataArray)
          
            })
            .on("error" ,function(err) {
                console.log(err);
            })
             .on("end", function(){
                // console.log(dataArray)
                createNewEntries(dataArray, function(err , res) {
                    if(err) console.log(err);
                    console.log(res);
                });
                 res.send({message : "Successfully Uploaded"})
                console.log("done");
            });
                    })   
            
                })
              }) 
              blobStream.end(req.file.buffer);
             
       

    },

    
    getuniquesimulationid : async (req, res, next) => {
        console.log("Getting Simulation ID");
        var option ={
            useNewUrlParser: true,
            socketTimeoutMS: 30000,
            keepAlive: true,
            reconnectTries: 30000,
            useNewUrlParser: true,
            reconnectTries: 60,
            reconnectInterval: 1000,
            poolSize: 10,
            bufferMaxEntries: 0
        }
        
        var dist = simulationSchema.aggregate([{ $group : {
           _id : null,
           label : {$first : "$simulation_name"},
           value : {$first : "$simulation_id"}
        }}])
        dist.exec((err , result) => {
           if(err) console.log(err)
           console.log(result)
           res.send(result)
        })
    },

    getallsimsdata : async (req, res, next) =>{
        console.log("Getting Data");
        var uri = credentials.url;
        const client = new mongodb(uri, option);
        client.connect((err,db) => {
            if(err) console.log(err)
         var DB =  db.db('argo');
             //Get the collection and bulk api artefacts
           var bulk =  DB.collection('simulations').initializeUnorderedBulkOp();
           bulk.find()
        })

    },


    
  }

  var createNewEntries = function(entries, callback) {

    console.log(entries)
    var uri = credentials.url;
    const client = new mongodb(uri, option);
    client.connect((err,db) => {
        if(err) console.log(err)
     var DB =  db.db('argo');
         //Get the collection and bulk api artefacts
    var collection = DB.collection('simulations'),          
    bulkUpdateOps = [];  
      

entries.forEach(function(doc) {
    bulkUpdateOps.push({ "insertOne": { "document": doc } });

    if (bulkUpdateOps.length === 10000) {
        collection.bulkWrite(bulkUpdateOps).then(function(r) {
            // do something with result
            console.log("Record to insert", bulkUpdateOps.length);
            console.log(r.insertedCount)
        });
        bulkUpdateOps = [];
    }
})

if (bulkUpdateOps.length > 0) {
    collection.bulkWrite(bulkUpdateOps).then(function(r) {
        // do something with result
        //sconsole.log(r);
        console.log(r.insertedCount)
    });
}
    })
    
};