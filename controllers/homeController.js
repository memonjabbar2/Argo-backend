const mongodb = require('mongodb').MongoClient;
var mongoose = require('mongoose');
var baiscInfo = require('./../models/basicinfoSchema.js');
const resinsurance_layer = require('../models/reinsuranceinputs');
var credentials = require('./../key.js')
var csv = require("fast-csv");
var fs = require('fs');
var stream = fs.createReadStream("./limit_amortization_layer_prepayments_only_deal_out_17HQA2_combine_state_50_80.csv")
var dataArray = []

var option = {
    useNewUrlParser: true,
    socketTimeoutMS: 30000,
    keepAlive: true,
    reconnectTries: 30000,
    useNewUrlParser: true,
    reconnectTries: 60,
    reconnectInterval: 1000,
    poolSize: 10,
    bufferMaxEntries: 0
}

module.exports = {
    getbasicinfo: async (req, res, next) => {
        console.log("Getting Data From Basic info");
        var baiscInfo = [];
        var uri = credentials.url;
        const client = new mongodb(uri, option);
        client.connect((err, db) => {
            if (err) console.log(err)
            //Database Name
            var DB = db.db('argo').collection('basicinfos').find({}).toArray(function (err, result) {
                if (err) throw err;
                console.log(result)
                res.send(result)
            })

        })
    },
    uploadresinsuranceresult: async (req, res, next) => {
        console.log("Uploading Reinsurance Result");
        csv
            .fromStream(stream, { headers: true })
            .on("data", (data) => {
                //console.log(data)
                //Pushing Every json to Array
              var obj ={};
            //   obj["tranches"] = data["tranches"];
            //   obj["P[Attachment]"] = data["P[Attachment]"];
            //   obj["baseline"] = data["baseline"];
            //   obj["1 in 10"] = data["1 in 10"]
            //   obj["1 in 1000"] = data["1 in 1000"];
            //   obj["1 in 5"] = data["1 in 5"];
            //   obj["1 in 250"] = data["1 in 250"];
            //   obj["1 in 50"] = data["1 in 50"];
            //   obj["1 in 100"] = data["1 in 100"];
            //   obj["1 in 2"] = data["1 in 2"];
            //   obj["1 in 500"] = data["1 in 500"];
            //   obj["deal_out_name"] = data["deal_out_name"];
            obj.scenario = data.scenario;
            obj.simulation = data.simulation;
            obj.limit_reduced_by_prepay_M1 = data.limit_reduced_by_prepay_M1;
            obj.limit_reduced_by_prepay_M2 = data.limit_reduced_by_prepay_M2;
            obj.limit_reduced_by_prepay_B1 = data.limit_reduced_by_prepay_B1;
            obj.limit_reduced_by_prepay_B2 = data.limit_reduced_by_prepay_B2;
            obj.cumloss= data.cumloss;
            obj.requi_collateral_reins_share_M1 = data.requi_collateral_reins_share_M1;
            obj.requi_collateral_reins_share_M2 = data.requi_collateral_reins_share_M2;
            obj.requi_collateral_reins_share_B1 = data.requi_collateral_reins_share_B1;
            obj.requi_collateral_reins_share_B2 = data.requi_collateral_reins_share_B2;
            obj.deal_out_name = data.deal_out_name
            obj["deal_id"] = "ofark7236"
              dataArray.push(obj);
                // dataArray.push({
                //     tranches : data.tranches,
                //     P_Attachment : data.P[Attachment],
                //     baseline : data.baseline,
                //     1in 1000

                //     deal_id: "ofark7236"
                // })
            })
            .on("error", function (err) {
                console.log(err);
            })
            .on("end", function () {
                console.log(dataArray)
                createNewEntries(dataArray, function (err, res) {
                    if (err) console.log(err);
                    console.log(res);
                });
                res.send({message : "Successfully Uploaded"})
                console.log("done");

            })



    },
    getpariculardeal: async (req, res, next) => {
        console.log("Getting Data");
        var deal_id = req.body.deal_id;
        if (!deal_id) {
            return res.status(404).send({ message: "Deal ID is Required" });
        }
        console.log("Deal ID==>", deal_id)
        var dealData = {};
        var uri = credentials.url;
        const client = new mongodb(uri, option);
        client.connect((err, database) => {
            if (err) console.log(err)

            baiscinfoData(deal_id, function (err, data) {
                //====================Getting Basic Info  ======================  
                if (err) console.log(err)
                dealData.basicInfo = data;
                //====================Getting Reinsurance results ======================
                reinsuranceResults(deal_id, function (err, result) {
                    if (err) console.log(err)
                    console.log(result);
                    dealData.reinsurance_result = result
                    //====================Getting Bar Chart  ======================  
                    Barchart(deal_id, function (err, bardata) {
                        if (err) console.log(err)
                        //console.log(bardata);
                        dealData.Barchart = bardata
                        //====================Getting USA MAP  ====================== 
                        usamap(deal_id, function (err, usadata) {
                            if (err) console.log(err)
                            console.log(usadata)
                            dealData.usaMap = usadata;
                            //====================Getting reinsurance Inputs  ====================== 
                            ReinsuranceInput(deal_id, function (err, reinsurance) {
                                if (err) console.log(err)
                                console.log(reinsurance)
                                dealData.resinsurance_layer = reinsurance
                                //console.log(dealData)
                                res.send(dealData)
                            })

                        })
                    })

                })

            })

        })
    },
    updatereinsurancelayer: async (req, res, next) => {
        console.log("Updating reinsurance layer");
        var layer = [];
        layer = req.body.reinsurance_layer;
        //console.log(layer)
        if (!layer) {
            return res.status(404).send({ message: "layer Are Required" });
        }
        // console.log(typeof(layer))
        var parseJson = JSON.parse(layer);
        console.log(parseJson)



    },
    getinsuranceresult : async (req, res, next) => {
        console.log("Getting Data");
        var dealid = req.body.deal_id;
        console.log("Deal Id" , dealid);

        if(!dealid) {
            return res.send("Deal Id should not be Null")
        }

        table1(dealid , function(err , table1Data) {
            if(err) console.error(err)
            console.log("TABLE 1 ===========")
            console.log(table1Data)
         //=========================== TABLE 2 =======================
            table2(dealid, function(err , table2Data) {
                if(err) console.error(err);
                console.log("TABlE 2 ======")
                console.log(table2Data);
                    chartData(dealid , function(err , chart_data) {
                        if(err) console.error(err)
                        console.log("CHART DATA ======");
                        console.log(chart_data);
                        var result = {};
                        result.table1 = table1Data;
                        result.table2 = table2Data;
                        result.chart = chart_data

                        res.send(result);
                    })
            })   
        })
    },
    scenatiobasedreinsuranceresult : async (req, res, next) =>{
        console.log("Getting data according Scenario");

        var scenario = req.body.scenario;
        console.log("Scenario == " , scenario);

        if(!scenario) {
            return res.send("Scenario cant be empty")
        }

        scenarioTable1(scenario , function(err , callback) {
            if(err) console.error(err)
            console.log(callback);
            scenarioChartData(scenario , function(err , chart_data) {
                if(err) console.error(err)
                console.log(chart_data);
                var result = {};
                result.table1 = callback;
                result.chart = chart_data
                res.send(result);
            })
        })
    }


}

var baiscinfoData = async function (deal_id, callback) {
    baiscInfo.find({ "deal_id": deal_id }, function (err, basicData) {
        if (err) console.log(err);
        console.log("Basic Info Data ==>");
        // console.log(basicData)
        callback(null, basicData)
    })
}

var reinsuranceResults = async function (deal_id, callback) {
    var uri = credentials.url;
    const client = new mongodb(uri, option);
    client.connect((err, database) => {
        if (err) console.log(err)

        database.db('argo').collection('reinsurance_results').find({ "deal_id": deal_id }).toArray((err, data) => {
            if (err) console.log(err)
            console.log("reinsurance results======>");
            //console.log(data)
            callback(null, data);
        })
    })
}

var Barchart = async function (deal_id, callback) {
    var uri = credentials.url;
    var barArray = [];
    const client = new mongodb(uri, option);
    client.connect((err, database) => {
        if (err) console.log(err)

        database.db('argo').collection('barchart').find({ "deal_id": deal_id }).toArray(function (err, bardata) {
            if (err) console.log(err)
            console.log("Bar Chart====>")
            var fico ={};
            var ltv = {};
            var dti = {};

            bardata.forEach((bar) => {
                fico = bar.fico_obj;
                ltv = bar.ltv_obj;
                dti = bar.dti_obj;
            })
            const fico_ordered = {};
            const ltv_ordered = {};
            const dti_ordered = {};

            Object.keys(fico).sort().reverse().forEach(function(key) {
                fico_ordered[key] = fico[key];
            })

            Object.keys(ltv).sort().reverse().forEach(function(key) {
                ltv_ordered[key] = ltv[key];
            })

            Object.keys(dti).sort().reverse().forEach(function(key) {
                dti_ordered[key] = dti[key];
            })
            // console.log(fico_ordered);
            // console.log(ltv_ordered);
            // console.log(dti_ordered);
            barArray.push({fico_obj : fico_ordered , ltv_obj : ltv_ordered , dti_obj : dti_ordered , deal_id : deal_id});
            //console.log(barArray)
            callback(null, barArray)
        })
    })
}

var usamap = async function (deal_id, callback) {
    var uri = credentials.url;
    const client = new mongodb(uri, option);
    client.connect((err, database) => {
        if (err) console.log(err)
        database.db('argo').collection('usamap').find({ "deal_id": deal_id }).toArray(function (err, usamap) {
            if (err) console.log(err)
            console.log("USA MAP ====>")
            //console.log(usamap);
            callback(null, usamap);
        })
    })
}

var ReinsuranceInput = async function (deal_id, callback) {
    resinsurance_layer.find({ "deal_id": deal_id }, function (err, reinsuranceinput) {
        if (err) console.log(err)
        console.log("Reinsurance resultsss ====>")
        callback(null, reinsuranceinput);
        //console.log(reinsurance);
    })
}

var createNewEntries = function (entries, callback) {

    //console.log(entries)
    var total;
    var uri = credentials.url;
    const client = new mongodb(uri, option);
    client.connect((err, db) => {
        if (err) console.log(err)
        var DB = db.db('argo');
        //Get the collection and bulk api artefacts
        var collection = DB.collection('chart_data'),
            bulkUpdateOps = [];


        entries.forEach(function (doc) {
            bulkUpdateOps.push({ "insertOne": { "document": doc } });
            if (bulkUpdateOps.length === 10000) {
                collection.bulkWrite(bulkUpdateOps).then(function (r) {
                    // do something with result
                    console.log("Record to insert", bulkUpdateOps.length);
                    // console.log(r.insertedCount)
                    //callback(null, r.insertedCount)
                }).catch((err) => {
                    console.log(err)
                })
                bulkUpdateOps = [];
            }
        })

        if (bulkUpdateOps.length > 0) {

            collection.bulkWrite(bulkUpdateOps).then(function (r) {
                console.log("Record to inserted less than 10000", r.insertedCount);
                //callback(null, r.insertedCount)
            }).catch((err) => {
                console.log(err)
            })
        }

    })

};

var table1 = async function(deal_id , callback) {
    console.log("getting data from table1");
    var uri = credentials.url;
    const client = new mongodb(uri, option);
    client.connect((err, db) => {
        if (err) console.log(err)
        //Database Name
        var DB = db.db('argo').collection('reinsurance_result_table1').find({'deal_id' : deal_id}).toArray(function (err, result) {
            if (err) throw err;
            //console.log(result)
            callback(null , result);
            //res.send(result)
        })

    })

}

var table2 = async function(deal_id , callback) {
    console.log("getting data from table2");
    var uri = credentials.url;
    const client = new mongodb(uri, option);
    client.connect((err, db) => {
        if (err) console.log(err)
        //Database Name
        var DB = db.db('argo').collection('reinsurance_result_table2').find({'deal_id' : deal_id}).toArray(function (err, result) {
            if (err) throw err;
            //console.log(result)
            callback(null , result);
            //res.send(result)
        })

    })
}

var chartData = async function(deal_id , callback) {
    console.log("getting chart data");
    var uri = credentials.url;
    const client = new mongodb(uri, option);
    client.connect((err, db) => {
        if (err) console.log(err)
        //Database Name
        var DB = db.db('argo').collection('chart_data').find({'deal_id' : deal_id}).toArray(function (err, result) {
            if (err) throw err;
            //console.log(result)
            callback(null , result);
            //res.send(result)
        })

    })
}

var scenarioTable1 = async function(scenario , callback) {
    console.log("getting data from table1");
    var uri = credentials.url;
    const client = new mongodb(uri, option);
    client.connect((err, db) => {
        if (err) console.log(err)
        //Database Name
        var DB = db.db('argo').collection('reinsurance_result_table1').find({'scenario' : scenario}).toArray(function (err, result) {
            if (err) throw err;
            //console.log(result)
            callback(null , result);
            //res.send(result)
        })

    })

}

var scenarioChartData = async function(scenario , callback) {
    console.log("getting chart data");
    var uri = credentials.url;
    const client = new mongodb(uri, option);
    client.connect((err, db) => {
        if (err) console.log(err)
        //Database Name
        var DB = db.db('argo').collection('chart_data').find({'scenario' : scenario}).toArray(function (err, result) {
            if (err) throw err;
            //console.log(result)
            callback(null , result);
            //res.send(result)
        })

    })
}