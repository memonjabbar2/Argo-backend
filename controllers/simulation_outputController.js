const express = require('express')
const bodyParser = require('body-parser');
const mongodb = require('mongodb').MongoClient;
var mongoose = require('mongoose');
var app = express();
var fs = require('fs');
var csv = require("fast-csv");
const simsoutputschema = require("../models/simulation_output_schema")
var credentials = require('./../key.js')
var stream = fs.createReadStream("./severe_1K_simulations_output_state_rates_v1.csv")
var file = "./severe_1K_simulations_output_state_rates_v1.csv"

const {Storage} = require('@google-cloud/storage');
const CLOUD_BUCKET = "portfolio_csv";
const googleCloudStorage =  new Storage({
    projectId: process.env.GCLOUD_STORAGE_BUCKET ||"argo-mortgage",
    keyFilename: process.env.GCLOUD_KEY_FILE || "./credentials.json"
  });
  
  const bucket = googleCloudStorage.bucket(CLOUD_BUCKET);
  var dataArray= [];
  var option ={
    useNewUrlParser: true,
    socketTimeoutMS: 30000,
    keepAlive: true,
    reconnectTries: 30000,
    useNewUrlParser: true,
    reconnectTries: 60,
    reconnectInterval: 1000,
    poolSize: 10,
    bufferMaxEntries: 0
}

module.exports = {
    uploadsimsoutput : async(req, res, next) => {
        console.log("uploading output");
             // Create a new blob in the bucket and upload the file data.
            //  const blob = bucket.file(file);
             
            
            //  // Make sure to set the contentType metadata for the browser to be able
            //  // to render the image instead of downloading the file (default behavior)
            //  const blobStream = blob.createWriteStream({
            //    metadata: {
            //      contentType: file
            //    }
            //  });
           
            //  blobStream.on("error", err => {
            //    next(err);
            //    return;
            //  });
           
            // await blobStream.on("finish", () => {
            //    // The public URL can be used to directly access the file via HTTP.
            //    const publicUrl = `https://storage.googleapis.com/${bucket.name}/${blob.name}`;
           
            //    // Make the image public to the web (since we'll be displaying it in browser)
            //    blob.makePublic().then(() => {
            //      //res.status(200).send(`Success!\n File uploaded to ${publicUrl}`);
            //      console.log(`Success!\n File uploaded to ${publicUrl}`)
            //      url = publicUrl
            //    }).then(() => {
                        
                   //Saving the file to local Storage
                //    fs.writeFile("upload_simulation_output_db.csv", req.file.buffer, function (err) {
                //        if (err) {
                //            return console.log(err);
                //        }
                //        console.log("The file was saved! on localhost");
                         
                   //Adding file Data to Array for inserting into DB.
                   var port_file =  fs.createReadStream("upload_simulation_db.csv")
                   csv
                       .fromStream(stream,{headers : true})
                       .on("data", (data) => {
                      // console.log(data)
                      dataArray.push({
                        default_probability : data.default_probability,
                        prepayment_probability : data.prepayment_probability,
                        simulation : data.simulation,
                        state : data.state,
                        year : data.year,
                        sims_name : data.sims_name,
                        simulation_id : "htjh8ix6o"
                      })
                      //console.log(dataArray)
         
           })
           .on("error" ,function(err) {
               console.log(err);
           })
            .on("end", function(){
              // console.log(dataArray)
               createNewEntries(dataArray)
                res.send({message : "Successfully Uploaded"})
               console.log("done");
           });
               //    })   
           
               //})
            // }) 
             //lobStream.end(req.file.buffer);
            
    }
}


var createNewEntries = function(entries) {
    console.log(entries) 
    var bulkUpdateOps = [];  
        
    var uri = credentials.url;
    const client = new mongodb(uri, option);
    client.connect((err,db) => {
        if(err) console.log(err)
     var DB =  db.db('argo');
         //Get the collection and bulk api artefacts
    var collection = DB.collection('simulation_outputs')  

entries.forEach(function(doc) {
    bulkUpdateOps.push({ "insertOne": { "document": doc } });

    

    if (bulkUpdateOps.length === 10000) {
        collection.bulkWrite(bulkUpdateOps).then(function(r) {
            // do something with result
            console.log("Record to insert", bulkUpdateOps.length);
           // console.log(r.insertedCount)
           // callback(null, r.insertedCount)
        }).catch((err) =>{
            console.log(err)
        })
        bulkUpdateOps = [];
    }


})

if (bulkUpdateOps.length > 0 ) {
    
    collection.bulkWrite(bulkUpdateOps).then(function(r) {       
        console.log("Record to inserted less than 10000",r.insertedCount);
       // callback(null, r.insertedCount)
    }).catch((err) => {
        console.log(err)
    })
}
    })
    
};