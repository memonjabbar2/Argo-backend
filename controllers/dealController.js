const express = require('express')
var Request = require("request").defaults({
    forever: true
});
const bodyParser = require('body-parser');
const mongodb = require('mongodb').MongoClient;
var mongoose = require('mongoose');
var app = express();
var fs = require('fs');
var csv = require("fast-csv");
const dealSchema = require("../models/dealSchema")
const basicinfoSchema = require('../models/basicinfoSchema.js');
const resinsurance_layer = require('../models/reinsuranceinputs');
var credentials = require('./../key.js')
var Layer = [
    {
        "tranche_name": "M1",
        "attachment": 0.0325,
        "collateral": 0.0425,
        "premium": 0.012,
        "rein_share": 0.02,
        "size_of_layers": 0.01,
        "detachment": 0.0425,
        "limit_for_everyone": 300566780,
        "limit_for_Argo": 6011335.600000001,
        "argo_premium": 72136.02720000001,
        "collateral_requi": 12774088.15,
        "collateral_total": 255481.763
    },
    {
        "tranche_name": "M2",
        "attachment": 0.01,
        "collateral": 0.075,
        "premium": 0.035,
        "rein_share": 0.011,
        "size_of_layers": 0.0225,
        "detachment": 0.0325,
        "limit_for_everyone": 676275255,
        "limit_for_Argo": 7439027.805,
        "argo_premium": 260365.97317500002,
        "collateral_requi": 50720644.125,
        "collateral_total": 557927.085375
    },
    {
        "tranche_name": "B1",
        "attachment": 0.005,
        "collateral": 0.18,
        "premium": 0.05,
        "rein_share": 0.008,
        "size_of_layers": 0.005,
        "detachment": 0.01,
        "limit_for_everyone": 150283390,
        "limit_for_Argo": 1202267.12,
        "argo_premium": 60113.35600000001,
        "collateral_requi": 27051010.2,
        "collateral_total": 216408.0816
    },
    {
        "tranche_name": "B2",
        "attachment": 0,
        "collateral": 0,
        "premium": 0,
        "rein_share": 0,
        "size_of_layers": 0.005,
        "detachment": 0.005,
        "limit_for_everyone": 150283390,
        "limit_for_Argo": 0,
        "argo_premium": 0,
        "collateral_requi": 0,
        "collateral_total": 0
    }
]
const { Storage } = require('@google-cloud/storage');
const CLOUD_BUCKET = "portfolio_csv";
const googleCloudStorage = new Storage({
    projectId: process.env.GCLOUD_STORAGE_BUCKET || "argo-mortgage",
    keyFilename: process.env.GCLOUD_KEY_FILE || "./credentials.json"
});

const bucket = googleCloudStorage.bucket(CLOUD_BUCKET);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
var dataArray = []
var option = {
    useNewUrlParser: true,
    socketTimeoutMS: 30000,
    keepAlive: true,
    reconnectTries: 30000,
    useNewUrlParser: true,
    reconnectTries: 60,
    reconnectInterval: 1000,
    poolSize: 10,
    bufferMaxEntries: 0
}


module.exports = {

    //Adding Deal
    adddeal: async (req, res, next) => {
        console.log("Adding Deal");
        var dealid = Math.random().toString(36).substr(2, 9)
        console.log("Deal ID", dealid)
        var url;
        //Getting input File

        if (!req.file) {

            res.status(400).send("No file uploaded.");
            return;
        }

        // Create a new blob in the bucket and upload the file data.
        const blob = bucket.file(req.file.originalname);

        // Make sure to set the contentType metadata for the browser to be able
        // to render the image instead of downloading the file (default behavior)
        const blobStream = blob.createWriteStream({
            metadata: {
                contentType: req.file.mimetype
            }
        });

        blobStream.on("error", err => {
            next(err);
            return;
        });

        await blobStream.on("finish", () => {
            // The public URL can be used to directly access the file via HTTP.
            const publicUrl = `https://storage.googleapis.com/${bucket.name}/${blob.name}`;

            // Make the image public to the web (since we'll be displaying it in browser)
            blob.makePublic().then(() => {
                //res.status(200).send(`Success!\n File uploaded to ${publicUrl}`);
                console.log(`Success!\n File uploaded to ${publicUrl}`)
                url = publicUrl
            }).then(() => {
                var simulation = req.body.simulation_name;
                if (!simulation) {
                    return res.status(404).send({ message: "Simulation Name field is Required" });
                }
                console.log("Simulation name==>", simulation);

                //Stress Factor Start
                var stress_factor_start = req.body.stress_factor_start;
                if (!stress_factor_start) {
                    return res.status(404).send({ message: "Stress Factor field is Required" });
                }
                console.log("Stress Factor Start", stress_factor_start);

                //Stress factor End
                var stress_factor_end = req.body.stress_factor_end;
                if (!stress_factor_end) {
                    return res.status(404).send({ message: "Stress Factor field is Required" });
                }
                console.log("Stress Factor Start", stress_factor_end);

                var provider = req.body.provider;
                if (!provider) {
                    return res.status(404).send({ message: "Provider field is Required" });
                }
                console.log("Provider", provider);

                var deal_output_name = req.body.deal_output_name;
                if (!deal_output_name) {
                    return res.status(404).send({ message: "Deal Output Name field is Required" });
                }

                var dealdate = req.body.deal_date;
                if (!dealdate) {
                    return res.status(404).send({ message: "Deal Date field is Required" });
                }


                // var dealmonth = req.body.deal_month;
                // if(!dealmonth) {
                //     return res.status(404).send({message : "Deal Month Name field is Required"});
                // }

                var reinsurancelayer = []
                //reinsurancelayer = Layer
                reinsurancelayer = (req.body.reinsurance_layer);
                //reinsurancelayer= req.body.reinsurancelayer;
                //reinsurancelayer.push(req.body.reinsurancelayer)
                console.log("Resinsurance Layer", reinsurancelayer)
                console.log("length", reinsurancelayer.length);

                var term = req.body.term;
                console.log("Term", term);

                var argopart = req.body.argopart;
                console.log("Argo Part", argopart);

                //Saving the file to local Storage
                fs.writeFile("run_deal.csv", req.file.buffer, function (err) {
                    if (err) {
                        return console.log(err);
                    }
                    console.log("The file was saved! on localhost");

                    //Adding file Data to Array for inserting into DB.
                    var port_file = fs.createReadStream("run_deal.csv")
                    csv
                        .fromStream(port_file, { headers: true })
                        .on("data", (data) => {
                            //console.log(data)
                            //Pushing Every json to Array
                            dataArray.push({
                                deal_id: dealid,
                                simulation_name: simulation,
                                provider: provider,
                                deal_date: dealdate,
                                id_loan: data.id_loan,
                                dt_first_pi: data.dt_first_pi,
                                orig_upb: Number(data.orig_upb),
                                last_upb: Number(data.last_upb),
                                fico: Number(data.fico),
                                cltv: Number(data.cltv),
                                ltv: Number(data.ltv),
                                dti: Number(data.dti),
                                mi_pct: Number(data.mi_pct),
                                int_rt: Number(data.int_rt),
                                loan_age: Number(data.loan_age),
                                flag_fthb: data.flag_fthb,
                                occpy_sts: data.occpy_sts,
                                st: data.st,
                                prop_type: data.prop_type,
                                loan_purpose: data.loan_purpose,
                                orig_loan_term: Number(data.orig_loan_term),
                                cnt_borr: Number(data.cnt_borr),
                                term: "Anually",
                                argo_part: "yes",
                                deal_output_name: deal_output_name
                            })
                        })
                        .on("error", function (err) {
                            console.log(err);
                        })
                        .on("end", function () {
                            // console.log(dataArray)

                            //Inserting Reinsurance Inputs
                            var parseJson = JSON.parse(reinsurancelayer);
                            console.log(parseJson.length)
                            if (parseJson.length != 0) {

                                parseJson.forEach((item) => {
                                    item.deal_id = dealid
                                    console.log("Deal ID==", item.deal_id);
                                })

                                //reinsurancelayer["deal_id"] = dealid;
                                //Saving to DB
                                resinsurance_layer.insertMany(parseJson, function (err, result) {
                                    if (err) console.log(err)
                                    //console.log(result)
                                    console.log("Reinsuarnce Input Saved to DB");
                                })
                            }
                            else {
                                console.log("there are no reinsurance layers");
                            }

                            var total = 0;
                            createNewEntries(dataArray, function (err, counts) {
                                if (err) console.log(err);
                                total += counts
                                console.log("Total ", total)
                                if (total === dataArray.length) {
                                    console.log("Calling Basic INfo Function");
                                    basicinfo(dealid, function (err, callback) {
                                        if (err) console.log(err)
                                        console.log(callback)
                                        ///Creating Collection for Barchart
                                        barchart(dealid, function (err, chartData) {
                                            if (err) console.error(err)
                                            console.log("Callback Data")
                                            console.log(chartData)
                                            if (chartData) {
                                                var chartArray = [];
                                                chartArray.push(chartData)
                                                console.log(chartArray)
                                                var uri = credentials.url;
                                                const client = new mongodb(uri, option);
                                                client.connect((err, db) => {
                                                    if (err) console.log(err)
                                                    var DB = db.db('argo');
                                                    DB.collection('barchart').insertMany(chartArray, function (err, chartRes) {
                                                        if (err) console.error(err)
                                                        console.log(chartRes.insertedIds)
                                                        ////============================= USA MAp Chart=====================================================
                                                        usaMap(dealid, function (err, callback) {
                                                            if (err) console.log(err);
                                                            console.log(callback)

                                                            //Mayur Api Remaning to Integrate==========================================================>
                                                            Request.post({
                                                                "headers": { "content-type": "application/json" },
                                                                "url": "http://35.236.78.73:5000/runsims",
                                                                "body": JSON.stringify({
                                                                    "dealfilepath": url,
                                                                    "deal_id": dealid,
                                                                    "sim_id": "htjh8ix6o",
                                                                    "deal_year": 2017,
                                                                    "deal_month": 2,
                                                                    "deal_out_name": deal_output_name,
                                                                    "simulation_start": 6,
                                                                    "simulation_end": 7,
                                                                    "stress_factor_start": stress_factor_start,
                                                                    "stress_factor_end": stress_factor_end
                                                                })
                                                            }, (error, response, body) => {
                                                                if (error) {
                                                                    return console.dir(error);
                                                                }
                                                                console.log(response);
                                                                //console.dir(JSON.parse(body));
                                                            });

                                                        })
                                                    })
                                                })
                                            }
                                            else {
                                                console.log("No Data Found");
                                            }
                                        });
                                    });

                                }

                            })
                            res.send({ message: "Successfully Uploaded" })
                            console.log("done");
                        });
                })

            })
        })
        blobStream.end(req.file.buffer);
    },

    updatetypeofdeal : async (req, res, next) => {
        console.log("Updating Type of Deal");
        var deal_id = req.body.deal_id;
        console.log("Deal ID == >" ,deal_id);

        var deal_type = req.body.deal_type;
        console.log("type of deal" , deal_type);

        if(!deal_id) {
            return res.status(400).send("Deal Id cannot be null")
        }

        if(!deal_type) {
            return res.send(400).send("Deal type cannot be null")
        }
        var condition = {deal_id : deal_id};
        var update = {deal_type : deal_type}
        dealSchema.update(condition ,update ,function(err , numberAffected, raw) {
            if(err) console.log(err)
            console.log(numberAffected);
            console.log(raw)
        })
    }
}


var createNewEntries = function (entries, callback) {

    //console.log(entries)
    var total;
    var uri = credentials.url;
    const client = new mongodb(uri, option);
    client.connect((err, db) => {
        if (err) console.log(err)
        var DB = db.db('argo');
        //Get the collection and bulk api artefacts
        var collection = DB.collection('deals'),
            bulkUpdateOps = [];


        entries.forEach(function (doc) {
            bulkUpdateOps.push({ "insertOne": { "document": doc } });



            if (bulkUpdateOps.length === 10000) {
                collection.bulkWrite(bulkUpdateOps).then(function (r) {
                    // do something with result
                    console.log("Record to insert", bulkUpdateOps.length);
                    // console.log(r.insertedCount)
                    callback(null, r.insertedCount)
                }).catch((err) => {
                    console.log(err)
                })
                bulkUpdateOps = [];
            }


        })

        if (bulkUpdateOps.length > 0) {

            collection.bulkWrite(bulkUpdateOps).then(function (r) {
                console.log("Record to inserted less than 10000", r.insertedCount);
                callback(null, r.insertedCount)
            }).catch((err) => {
                console.log(err)
            })
        }

    })

};

///Creating  Basic Info Collection
var basicinfo = function (deal_id, callback) {
    console.log("Deal iD", deal_id)
    var size_layer;
    resinsurance_layer.find({ 'deal_id': deal_id }, function (err, docs) {
        if (err) console.log(err)
        //console.log("=========Printing Data========")
        //console.log(docs)

    }).then(() => {
        resinsurance_layer.aggregate([
            {
                $match: {
                    deal_id: deal_id
                }
            },
            {
                $group: {
                    _id: "$deal_id",
                    size_of_layers: { $sum: "$size_of_layers" }
                }
            }
        ]).exec((err, sumoflayer) => {
            if (err) console.log(err)
            console.log("Getting Total Size of Layer", sumoflayer)
            sumoflayer.forEach((items) => {
                size_layer = items.size_of_layers;
                size_layer = size_layer.toFixed(3);
                // console.log(size_layer)
            })

        })
    }).then(() => {
        dealSchema.aggregate([
            {
                $match: {
                    deal_id: deal_id
                }
            },
            {
                $group: {
                    _id: null,
                    avg_LTV: { $avg: "$ltv" },
                    avg_DTI: { $avg: "$dti" },
                    avg_FICO: { $avg: "$fico" },
                    total_loans: { $sum: 1 },
                    upb: { $sum: "$orig_upb" },
                    provider: { "$first": "$provider" },
                    term: { "$first": "$term" },
                    argo_part: { "$first": "$argo_part" },
                    deal_id: { $first: "$deal_id" },
                    deal_output_name: { $first: "$deal_output_name" },
                    avg_Total_Loan: { $avg: "$orig_upb" },
                    deal_date: { $first: "$deal_date" }

                },
            }
        ]).allowDiskUse(true)
            .exec((err, result) => {
                if (err) console.log(err)
                //console.log(result)



                result.forEach((limit) => {
                    limit.total_limit = (limit.upb * size_layer)
                    console.log("Total Limit", limit.total_limit)
                })
                basicinfoSchema.insertMany(result, function (err, basicData) {
                    if (err) console.log(err)

                    console.log("Basic Info Data", basicData)
                    callback("SuccessFully Uploaded basic Info")

                })

            })
    })
}

//Creating Barchart Collection

var barchart = async function (deal_id, callback) {
    console.log("Deallll ID =>", deal_id);
    var fico_obj = {};
    var total_rows = 0;
    var fico_chart = [];
    var a;
    var ltv_obj = {};
    var dti_obj = {};
    //#######################################    FICO #################################################################################  
    var range = [
        "0-600",
        "600-650",
        "650-700",
        "700-750",
        "750-800",
        "800-1200",
    ]
    // countrows(dealid);
    var ltv_range = [
        "0-70",
        "70-80",
        "80-85",
        "85-90",
        "90-95",
        "95-110"
    ]

    var dti_range = [
        "0-20",
        "20-30",
        "30-35",
        "35-40",
        "40-50",
        "50-70"
    ]

    
   
    await dealSchema.collection.find({ "deal_id": deal_id }).count().then((rows) => {
        console.log(rows)
        total_rows = rows;
    }).then(() => {
        var counter = 0;
        setTimeout(() => {
            range.forEach((rangeList) => {


                var arr = rangeList.split("-");
                console.log(arr[0] + " " + arr[1]);
                var lt = Number(arr[0]);
                var gt = Number(arr[1]);
                dealSchema.aggregate([
                    {
                        $match: {
                            fico: {
                                $lte: gt,
                                $gt: lt
                            },
                            deal_id: {
                                $eq: deal_id
                            }
                        }
                    },
                    {
                        $group: {
                            _id: deal_id,
                            total: { $sum: 1 }
                        }
                    }


                ]).exec((err, data) => {
                    //Counting the length of Data
                    counter++;
                    //console.log("Counter  =" , counter)
                    if (err) console.log(err)

                    if (data.length === 0) {
                        console.log("No record for", lt, " ", gt, "===", data);
                        fico_obj[rangeList] = 0;
                    }
                    else {
                        console.log("Fico between", lt, " ", gt, "is");
                        console.log(data)
                        data.forEach((item) => {
                            var ans = item.total / total_rows;
                            ans = ans * 100;
                            //console.log(ans)
                            var round_result = Number(Math.round(ans + 'e' + 1) + 'e-' + 1);
                            console.log(round_result)
                            fico_chart[rangeList] = round_result
                            fico_obj[rangeList] = round_result;
                            if (counter === range.length) {

                                console.log(fico_obj)
                            }
                        })
                    }
                })
            })
        }, 1000)
    }).then(() => {
        //#######################################    LTV   #################################################################################  
        var ltv_counter = 0;
        setTimeout(() => {
            ltv_range.forEach((rangeList) => {



                var arr = rangeList.split("-");
                console.log(arr[0] + " " + arr[1]);
                var lt = Number(arr[0]);
                var gt = Number(arr[1]);
                dealSchema.aggregate([
                    {
                        $match: {
                            ltv: {
                                $lte: gt,
                                $gt: lt
                            },
                            deal_id: {
                                $eq: deal_id
                            }
                        }
                    },
                    {
                        $group: {
                            _id: deal_id,
                            total: { $sum: 1 }
                        }
                    }


                ]).exec((err, data) => {
                    //Counting the length of Data
                    ltv_counter++;
                    console.log("Ltv Counter  =", ltv_counter)
                    if (err) console.log(err)

                    if (data.length === 0) {
                        console.log("No record for", lt, " ", gt, "===", data);
                        ltv_obj[rangeList] = 0;
                        console.log(ltv_obj)
                    }
                    else {
                        console.log("LTV between", lt, " ", gt, "is");
                        console.log(data)
                        data.forEach((item) => {
                            var ans = item.total / total_rows;
                            ans = ans * 100;
                            //console.log(ans)
                            var round_result = Number(Math.round(ans + 'e' + 1) + 'e-' + 1);
                            console.log(round_result)
                            ltv_obj[rangeList] = round_result
                            if (ltv_counter === ltv_range.length) {

                                //console.log(fico_chart)

                                console.log(ltv_obj)
                                //callback("LTV data" , ltv_obj)
                            }


                            //console.log("Counter ",counter)

                        })
                    }
                })
            })
        }, 1000)
    }).then(() => {
        var dti_counter = 0;
        console.log("Dti")
        setTimeout(() => {
            dti_range.forEach((rangeList) => {


                var arr = rangeList.split("-");
                console.log(arr[0] + " " + arr[1]);
                var lt = Number(arr[0]);
                var gt = Number(arr[1]);
                dealSchema.aggregate([
                    {
                        $match: {
                            dti: {
                                $lte: gt,
                                $gt: lt
                            },
                            deal_id: {
                                $eq: deal_id
                            }
                        }
                    },
                    {
                        $group: {
                            _id: deal_id,
                            total: { $sum: 1 }
                        }
                    }


                ]).exec((err, data) => {
                    //Counting the length of Data
                    dti_counter++;
                    console.log("DTI Counter  =", dti_counter)
                    if (err) console.log(err)

                    if (data.length === 0) {
                        console.log("No record for", lt, " ", gt, "===", data);
                        dti_obj[rangeList] = 0;
                        if (dti_counter === dti_range.length) {

                            //console.log(fico_chart)

                            console.log(dti_obj)
                            callback("Data", { fico_obj, ltv_obj, dti_obj, deal_id: deal_id })
                        }
                    }
                    else {


                        console.log("DTI between", lt, " ", gt, "is");
                        console.log(data)
                        data.forEach((item) => {
                            var ans = item.total / total_rows;
                            ans = ans * 100;
                            //console.log(ans)
                            var round_result = Number(Math.round(ans + 'e' + 1) + 'e-' + 1);
                            console.log(round_result)
                            dti_obj[rangeList] = round_result
                            console.log(dti_range.length)
                            if (dti_counter === dti_range.length) {

                                //console.log(fico_chart)

                                console.log(dti_obj)
                                callback("Data", { fico_obj, ltv_obj, dti_obj, deal_id: deal_id })
                            }


                            //console.log("Counter ",counter)

                        })
                    }
                })
            })
        }, 1000)
    })
        .catch((err) => {
            console.log(err)
        })

    // #######################################    DTI   #################################################################################  

    await dealSchema.collection.find({ "deal_id": deal_id }).count().then((rows) => {
        console.log(rows)
        total_rows = rows;
    }).then(() => {
        var dti_counter = 0;
        console.log("Dti")
        dti_range.forEach((rangeList) => {

            var arr = rangeList.split("-");
            console.log(arr[0] + " " + arr[1]);
            var lt = Number(arr[0]);
            var gt = Number(arr[1]);
            dealSchema.aggregate([
                {
                    $match: {
                        dti: {
                            $lte: gt,
                            $gt: lt
                        },
                        deal_id: {
                            $eq: deal_id
                        }
                    }
                },
                {
                    $group: {
                        _id: deal_id,
                        total: { $sum: 1 }
                    }
                }


            ]).exec((err, data) => {
                //Counting the length of Data
                dti_counter++;
                console.log("Ltv Counter  =", dti_counter)
                if (err) console.log(err)

                if (data.length === 0) {
                    console.log("No record for", lt, " ", gt, "===", data);
                    dti_obj[rangeList] = 0;
                }
                else {
                    console.log("LTV between", lt, " ", gt, "is");
                    console.log(data)
                    data.forEach((item) => {
                        var ans = item.total / total_rows;
                        ans = ans * 100;
                        //console.log(ans)
                        var round_result = Number(Math.round(ans + 'e' + 1) + 'e-' + 1);
                        console.log(round_result)
                        dti_obj[rangeList] = round_result
                        if (dti_counter === dti_range.length) {

                            //console.log(fico_chart)

                            console.log(dti_obj)
                        }


                        //console.log("Counter ",counter)

                    })
                }
            })
        })

    })



}

var usaMap = async function (deal_id, callback) {
    console.log("Deal ID for USa MAp", deal_id);
    dealSchema.aggregate([
        {
            $match: {
                deal_id: {
                    $eq: deal_id
                }
            }
        },

        {
            $group: {
                _id: "$st",
                total: { $sum: "$orig_upb" }
            }
        },



    ]).sort({ _id: 1 })
        .exec((err, data) => {
            if (err) console.error(err)
            console.log(data)
            var stateObj = {};
            data.forEach((stateData) => {
                stateObj[stateData._id] = stateData.total;
            })

            //adding deal_id to Object
            stateObj["deal_id"] = deal_id;
            console.log(stateObj)
            var uri = credentials.url;
            const client = new mongodb(uri, option);
            client.connect((err, db) => {
                if (err) console.log(err)
                var arrData = [];
                arrData.push(stateObj);
                //arrData["deal_id"] = deal_id;
                var DB = db.db('argo');
                DB.collection('usamap').insertMany(arrData, function (err, usamap) {
                    if (err) console.error(err)
                    console.log(usamap.insertedIds)
                    callback(null, "successfully uploaded");

                })
            })

        })

}