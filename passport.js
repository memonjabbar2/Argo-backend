const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const LocalStartegy = require('passport-local').Strategy;
const key = require('./key.js');
const User = require('./models/user.js');


//JSON Web Token Strategy
passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey : key.secret
}, async (payload , done) => {
    try {
        //Find the user specified in the token
        const user = await User.findById(payload.sub)
       
        //if user doesnt exist, handle it
        if(!user) {
            return done(null , false);
        }
        
        //otherwise return the user
        return done(null , user)
    } catch(error) {
        done(error , false)
    }

}))

//Local Strategy
passport.use(new LocalStartegy({
    usernameField : 'email'
} , async (email , password , done) => {
    try {
        //find the user with Given Email
    const user = await User.findOne({email : email})

    //if not , we have to handle it
    if(!user) {
        return done(null , false)
    }

    //check if the password is correct 
   const isMatch = await user.isValidPassword(password);
   
    //if not we have to handle it
    if(!isMatch) {
       return done(null , false)
    }
    //otherwise return the user
    return done(null, user)
    }catch(error) {
        done(error , false)
    }
    
}))
