var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var inputs = new Schema({
    tranche_name : {
        type : String
    },
    attachment : {
        type : Number
    },
    collateral : {
        type : Number
    },
    premium : {
        type : Number
    },
    rein_share : {
        type : Number
    },
    size_of_layers : {
        type : Number
    },
    detachment : {
        type : Number
    },
    limit_for_everyone : {
        type : Number
    },
    limit_for_Argo : { 
        type : Number
    },
    argo_premium : {
        type : Number
    },
    collateral_requi : {
        type : Number
    },
    collateral_total : {
        type : Number
    },
    deal_id : {
        type : String
    }
})


module.exports = mongoose.model('reinsurance_input' , inputs);