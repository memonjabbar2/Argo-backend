var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var dealSchema = new Schema({
    port_name : {
        type : String
    },
    id_loan : {
        type : String
    },
    dt_first_pi: {
        type : String
    },
    orig_upb : {
        type : Number
    },
    last_upb : {
        type : Number
    },
    fico : {
        type : Number
    },
    cltv : { 
        type : Number
    },
    ltv : {
        type : Number
    },
    dti : {
        type : Number
    },
    mi_pct : {
        type : Number
    },
    int_rt : {
        type : Number
    },
    loan_age : {
        type : Number
    },
    flag_fthb : { 
        type : String
    },
    occpy_sts : {
        type : String
    },
    st : {
        type : String
    },
    prop_type : {
        type : String
    },
    loan_purpose : {
        type : String
    },
    orig_loan_term : {
        type : Number
    },
    cnt_borr : {
        type : Number
    },
    term : {
        type : String
    },
    total_limit : {
        type : Number
    },
    argo_part : {
        type : String
    },
    deal_output_name : {
        type : String
    },
    deal_date : {
        type : String
    }
})

module.exports = mongoose.model('deal' , dealSchema);