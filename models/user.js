var mongoose = require('mongoose');
const bycrpt = require('bcryptjs');
const Schema = mongoose.Schema;

//Create a Schema
const userSchema = new Schema({
  email :{
    type : String,
    required : true,
    unique : true,
  },
  password : {
    type : String,
    required : true
  },
  resetPasswordToken: String,
  resetPasswordExpires: Date,
  firstname : String,
  lastname : String,
  contactno : Number,
  books : Array
  // name : String
});

userSchema.pre('save',async function(next) {
  try {
    //Generate Salt
   const salt =  await bycrpt.genSalt(10);
   //Generate the password (salt + hash)
   const hashPassword = await bycrpt.hash(this.password , salt);
  //  console.log('salt' , salt)
  //  console.log('hashed password', hashPassword)
  this.password = hashPassword;
  next();
  }catch(error) {
    next(error)
  }
})

userSchema.methods.isValidPassword = async function(newPassword) {
  try {
    return await bycrpt.compare(newPassword , this.password)
  }catch(error) {
    throw new Error(error)
  }
}
//Create a Model
const User = mongoose.model('user', userSchema);

//Export the model
module.exports = User;