var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var loanSchema = new Schema({
    port_name : {
        type : String
    },
    port_year : {
        type : Number
    },
    state : {
        type : String
    },
    total_loan_counts : {
        type : Number
    },
    total_orig_upb : {
        type : Number
    },
    avg_orig_upb : {
        type : Number
    },
    avg_ltv : {
        type : Number
    },
    avg_fico : {
        type : Number
    },
    avg_cltv : {
        type :Number
    },
    avg_dti : {
        type : Number
    },
    avg_int_rt : {
        type : Number
    },
    avg_mi_pct : {
        type : Number
    },
    avg_loan_age : {
        type : Number
    },
    flag_fthb : {
        N_counts : Number,
        X_counts : Number,
        Y_counts : Number
    },
    occpy_sts : {
        I_counts : Number,
        P_counts : Number,
        S_counts : Number
    },
    prop_type : {
        CO_counts : Number,
        CP_counts : Number,
        MH_counts : Number,
        PU_counts : Number,
        SF_counts : Number,
    },
    loan_purpose : {
        C_counts : Number,
        N_counts : Number,
        P_counts : Number,
        R_counts : Number
    }
})

module.exports = mongoose.model('loanComparison' , loanSchema);