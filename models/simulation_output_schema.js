var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var simulation_output = new Schema({
    simulation_id : {
        type : String
    },
    default_probability : {
        type : Number
    },
    prepayment_probability : {
        type : Number
    },
    simulation : {
        type : Number
    },
    state : {
        type : String
    },
    year : {
        type : Number
    },
    sims_name : {
        type : String
    }
})

module.exports = mongoose.model('simulation_output' , simulation_output);