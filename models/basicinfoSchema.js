var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var basicInfo = new Schema({
    provider : {
        type : String
    },
    dealID : {
        type : String
    },
    year : {
        type : String
    },
    upb : {
        type : Number
    },
    term : {
        type : String
    },
    total_limit : {
        type : Number
    },
    argo_part : {
        type : String
    },
    total_loans : {
        type : Number
    },
    avg_LTV : {
        type : Number
    },
    avg_DTI : {
        type : Number
    },
    avg_FICO : {
        type : Number
    },
    deal_id :{
        type : String
    },
    deal_output_name : {
        type : String
    },
    avg_Total_Loan : {
        type : Number
    },
    deal_date : {
        type : String
    }
})

module.exports = mongoose.model('basicinfo' , basicInfo);