var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var simulationSchema = new Schema({
    year : {
        type : Number
    },
    simulation : {
        type : Number
    },
    housing_price_index : {
        type : Number
    },
    unemployment_rate : {
        type : Number
    },
    
    year_30_mortage_rate : {
        type : Number
    },
    simulation_id : {
        type : String
    }

})

module.exports = mongoose.model('simulation' , simulationSchema);
