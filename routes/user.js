var express = require('express')
var app = express();
const passport = require('passport')
const passportConfig = require('./../passport.js')
const router = express.Router();

const { validateBody, schemas } = require('./../helpers/routeHelpers.js');
const UserController = require('./../controllers/users.js');


router.route('/signup')
.post(validateBody(schemas.authSchema) ,UserController.signUp);

router.route('/signin')
.post(validateBody(schemas.authSchema) ,passport.authenticate('local', {session : false}), UserController.signIn);

router.route('/secret')
.get(passport.authenticate('jwt' , {session : false}) ,UserController.secret);

router.route('/forgot')
.post(UserController.forgot);

router.route('/reset')
.post(UserController.reset)

module.exports = router;