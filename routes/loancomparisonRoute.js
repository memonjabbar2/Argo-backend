var express = require('express')
const router = express.Router();

const portfolioController = require('./../controllers/loancomparison.js');

//Adding Portfolio to Database
router.route('/addLoan')
.post(portfolioController.addLoan);

router.route('/loanComparison')
.post(portfolioController.loanComparison);

module.exports = router;
