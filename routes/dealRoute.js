var express = require('express')
const router = express.Router();
const {Storage} = require('@google-cloud/storage');
const CLOUD_BUCKET = "portfolio_csv";
const multer = require('multer');
const bodyParser = require('body-parser');
var app =express();
const googleCloudStorage =  new Storage({
    projectId: process.env.GCLOUD_STORAGE_BUCKET ||"argo-mortgage",
    keyFilename: process.env.GCLOUD_KEY_FILE || "./credentials.json"
  });
  
  const bucket = googleCloudStorage.bucket(CLOUD_BUCKET);
const m = multer({
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
  }));

  //Importing Controller
  var dealroute = require('./../controllers/dealController.js');

  //Creating Routes

  router.route('/addDeal')
  .post(m.single('file') ,dealroute.adddeal);

  router.route('/updateTypeDeal')
  .post(dealroute.updatetypeofdeal);

  module.exports = router;
