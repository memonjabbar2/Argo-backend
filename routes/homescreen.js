var express = require('express')
const router = express.Router();


//Importing Controller
var homeController = require('./../controllers/homeController');

//Creating Routes
router.route("/getBasicInfo")
.post(homeController.getbasicinfo)

router.route("/uploadReinsuranceResults")
.post(homeController.uploadresinsuranceresult);

router.route("/getParticularDealData")
.post(homeController.getpariculardeal)

router.route("/updateReinsuranceLayer")
.post(homeController.updatereinsurancelayer);

router.route("/getReinsuranceResult")
.post(homeController.getinsuranceresult);

router.route("/scenarioBasedReinsuranceResult")
.post(homeController.scenatiobasedreinsuranceresult);


module.exports = router;