'use strict';

const nconf = require('nconf');
var express = require('express')
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var morgan = require('morgan');

var app = express();
var key = require('./key.js');

//Importing Routes
var Loan = require('./routes/loancomparisonRoute.js');
var userRoute = require('./routes/user.js')
var simulations = require('./routes/simulationRoute.js')
var dealRoute = require('./routes/dealRoute.js');
var homeRoute = require('./routes/homescreen');
var sims_output = require('./routes/simulation_outputRoute');
//connecting database
mongoose.Promise = global.Promise;
mongoose.connect(key.url,{ useNewUrlParser: true })
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(morgan('dev'));
app.use('/users',userRoute)
app.use('/loan' , Loan);
app.use('/simulation', simulations)
app.use('/deal', dealRoute)
app.use('/home', homeRoute);
app.use('/simulationOutput' , sims_output)
// Read in keys and secrets. Using nconf use can set secrets via
// environment variables, command-line arguments, or a keys.json file.
nconf.argv().env().file('keys.json');

app.get('/',function(req,res){
  res.sendFile(__dirname + "/index.html");
});
app.listen(process.env.PORT || 8080 , () => {
    console.log('started web process at Port 8080');
  });
